<?php

namespace ActualMailer;

use Exception;
use GuzzleHttp\Client;

class Mail
{
    /**
     * @var $subject
     */
    protected $subject;

    /**
     * @var $from_mail
     */
    protected $from_mail = 'contato@contato-certo.com';

    /**
     * @var $from_name
     */
    protected $from_name = 'Contato';

    /**
     * @var $to
     */
    protected $to = [];

    /**
     * @var $cc
     */
    protected $cc = [];

    /**
     * @var $bcc
     */
    protected $bcc = [];

    /**
     * @var $body
     */
    protected $body;

    /**
     * @var $queue
     */
    protected $queue = 'default';

    /**
     * @var Replacement $replacements []
     */
    protected $replacements;

    /**
     * @var Attachment $attachments []
     */
    protected $attachments = [];

    /**
     * @var string $tag
     */
    protected $tag;

    /**
     * @var string $scheduledTo
     */
    protected $scheduledTo;

    /**
     * @var string $apiKey
     */
    protected $apiKey;

    /**
     * @var string $endPoint
     */
    protected $endPoint;

    public function __construct($data = [])
    {
        $this->set_object_vars($data);
    }

    public function set_object_vars($data)
    {
        if (isset($data['replacements'])) {
            foreach ($data['replacements'] as $replacement) {
                $this->addReplacement($replacement['search'], $replacement['replace']);
            }
            unset($data['replacements']);
        }
        if (isset($data['attachments'])) {
            foreach ($data['attachments'] as $attachment) {
                $this->addAttachment($attachment['filename'], $attachment['file'], false);
            }
            unset($data['attachments']);
        }
        $has = get_object_vars($this);
        foreach ($has as $name => $oldValue) {
            $this->$name = isset($data[$name]) ? $data[$name] : $oldValue;
        }
    }

    /**
     * @param $search string Search for
     * @param $replace string Replace with
     * @return $this
     */
    public function addReplacement($search, $replace)
    {
        $this->replacements[] = new Replacement($search, $replace);
        return $this;
    }

    /**
     * @param $filename string File name
     * @param $file
     * @param bool $fromPath
     * @return $this
     * @throws Exception
     */
    public function addAttachment($filename, $file, $fromPath = true)
    {
        if($fromPath) {
            $file = base64_encode(file_get_contents($file));
        } else {
            if (base64_decode($file, true) === false)
            {
                throw new Exception('The file needs to be Base64 Encoded or you should pass in the filepath');
            }
        }
        $this->attachments[] = new Attachment($filename, $file);
        return $this;
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function send()
    {
        $client = new Client();
        $res = $client->request('POST', $this->getEndPoint(), [
            'headers' => [
                'Authorization' => 'Bearer: ' . $this->getApiKey(),
                'accept' => 'application/json',
                'content-type' => 'application/json'
            ],
            'json' => $this->toArray()
        ]);
        return \GuzzleHttp\json_decode($res->getBody());
    }

    public function toArray()
    {
        $attachments = [];
        $replacements = [];
        foreach ($this->attachments as $attachment) {
            $attachments[] = $attachment->toArray();
        }
        foreach ($this->replacements as $replacement) {
            $replacements[] = $replacement->toArray();
        }
        $array = [
            'to' => $this->to,
            'from_name' => $this->from_name,
            'from_mail' => $this->from_mail,
            'subject' => $this->subject,
            'cc' => $this->cc,
            'bcc' => $this->bcc,
            'body' => $this->body,
            'queue' => $this->queue,
            'replacements' => $replacements,
            'attachments' => $attachments
        ];
        if (!empty($this->tag)) {
            $array['tag'] = $this->tag;
        }
        if (!empty($this->scheduledTo)) {
            $array['scheduled_to'] = $this->scheduledTo;
        }
        return $array;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFromMail()
    {
        return $this->from_mail;
    }

    /**
     * @param mixed $from_mail
     * @return $this
     */
    public function setFromMail($from_mail)
    {
        $this->from_mail = $from_mail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFromName()
    {
        return $this->from_name;
    }

    /**
     * @param mixed $from_name
     * @return $this
     */
    public function setFromName($from_name)
    {
        $this->from_name = $from_name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param mixed $cc
     * @return $this
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param mixed $bcc
     * @return $this
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQueue()
    {
        return $this->queue;
    }

    /**
     * @param mixed $queue
     * @return $this
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndPoint()
    {
        return $this->endPoint;
    }

    /**
     * @param string $endPoint
     * @return $this
     */
    public function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;
        return $this;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return string
     */
    public function getScheduledTo()
    {
        return $this->scheduledTo;
    }

    /**
     * @param string $scheduledTo
     * @return $this
     */
    public function scheduleTo($scheduledTo)
    {
        $this->scheduledTo = $scheduledTo;
        return $this;
    }


}