<?php

namespace ActualMailer;

class Attachment
{
    /**
     * @var $filename string;
     */
    protected $filename;

    /*
    * @var $file string;
    */
    protected $file;

    public function __construct($filename, $file)
    {
        $this->filename = $filename;
        $this->file = $file;
    }

    public function toArray()
    {
        return [
            'filename' => $this->filename,
            'file' => $this->file
        ];
    }

}