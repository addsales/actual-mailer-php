<?php

namespace ActualMailer;

class Replacement
{
    /**
    * @var $search;
    */
    protected $search;

    /**
    * @var $replace;
    */
    protected $replace;

    public function __construct($search, $replace)
    {
        $this->search = $search;
        $this->replace = $replace;
    }

    public function toArray()
    {
        return [
            'search' => $this->search,
            'replace' => $this->replace
        ];
    }
}