<?php

namespace ActualMailer;

use Exception;
use GuzzleHttp\Client;

class Batch
{

    /**
     * @var Mail[]
     */
    protected $batch = [];

    /**
     * @var string $scheduledTo
     */
    protected $endPoint;

    /**
     * @var string $apiKey
     */
    protected $apiKey;

    /**
     * @param Mail $mail
     * @return $this
     */
    public function addToBatch(Mail $mail)
    {
        $this->batch[] = $mail;
        return $this;
    }

    public function send()
    {
        $client = new Client();
        $res = $client->request('POST', $this->getEndPoint(), [
            'headers' => [
                'Authorization' => 'Bearer: ' . $this->getApiKey(),
                'accept' => 'application/json',
                'content-type' => 'application/json'
            ],
            'json' => $this->toArray()
        ]);
        return \GuzzleHttp\json_decode($res->getBody());
    }

    public function toArray()
    {
        $mailArray['batch'] = [];
        foreach ($this->batch as $mail) {
            $mailArray['batch'][] = $mail->toArray();
        }
        return $mailArray;
    }

    /**
     * @return string
     */
    public function getEndPoint()
    {
        return $this->endPoint;
    }

    /**
     * @param string $endPoint
     * @return $this
     */
    public function setEndPoint($endPoint)
    {
        $this->endPoint = $endPoint;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return Mail[]
     */
    public function getBatch()
    {
        return $this->batch;
    }

}