<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers \ActualMailer\Mail;
 */
final class MailTest extends TestCase
{
    public function testCanBeFilledWithArrayData()
    {
        $mail = new \ActualMailer\Mail([
            'to' => ['guilherme.assemany@actualsales.com.br'],
            'from_name' => 'Contato',
            'from_mail' => 'contato@contato-certo.com',
            'cc' => ['cc@actualsales.com.br'],
            'bcc' => ['bcc@actualsales.com.br'],
            'subject' => 'Test Subject',
            'body' => 'Custom Body',
            'queue' => 'default',
            'replacements' => [
                [
                    'search' => '{{nome}}',
                    'replace' => 'Guilherme'
                ],
                [
                    'search' => '{{cidade}}',
                    'replace' => 'Salvador'
                ]
            ],
            'attachments' => [
                [
                    'filename' => 'teste.txt',
                    'file' => 'YmFzZTY0ZW5jb2Rlcw=='
                ]
            ]
        ]);
        $this->assertInstanceOf(\ActualMailer\Mail::class, $mail);
        $this->assertAttributeEquals(['guilherme.assemany@actualsales.com.br'], 'to', $mail);
        $this->assertAttributeEquals('Contato', 'from_name', $mail);
        $this->assertAttributeEquals('contato@contato-certo.com', 'from_mail', $mail);
        $this->assertAttributeEquals(['cc@actualsales.com.br'], 'cc', $mail);
        $this->assertAttributeEquals(['bcc@actualsales.com.br'], 'bcc', $mail);
        $this->assertAttributeEquals('Test Subject', 'subject', $mail);
        $this->assertAttributeEquals('Custom Body', 'body', $mail);
        $this->assertAttributeEquals('default', 'queue', $mail);
    }

    public function testCanBeInstantiatedFluently()
    {
        $mail = new ActualMailer\Mail();
        $mail->setSubject('Olá {{nome}}')
            ->setQueue('default')
            ->setTo(['guilherme.assemany@actualsales.com.br'])
            ->setBcc([])
            ->setCc([])
            ->setBody('Hello, {{nome}}')
            ->addReplacement('{{nome}}', 'Guilherme');
        $this->assertInstanceOf(\ActualMailer\Mail::class, $mail);
    }

}