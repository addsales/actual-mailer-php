<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * @covers \ActualMailer\Mail;
 */
final class BatchMailTest extends TestCase
{
    public function testBatchEmailSend()
    {
        $batch = new \ActualMailer\Batch();
        $batch->setEndPoint('xxx');
        $batch->setApiKey('xxx');
        for ($x = 0; $x <= 1; $x++) {
            $mail = new \ActualMailer\Mail([
                'to' => ['guilherme.assemany@actualsales.com.br'],
                'from_name' => 'Contato',
                'from_mail' => 'contato@contato-certo.com',
                'cc' => [],
                'bcc' => [],
                'subject' => 'Test Subject',
                'body' => 'Custom Body',
                'queue' => 'default',
                'replacements' => [
                    [
                        'search' => '{{nome}}',
                        'replace' => 'Guilherme'
                    ],
                    [
                        'search' => '{{cidade}}',
                        'replace' => 'Salvador'
                    ]
                ],
                'attachments' => [
                    [
                        'filename' => 'teste.txt',
                        'file' => 'YmFzZTY0ZW5jb2Rlcw=='
                    ]
                ]
            ]);
            $batch->addToBatch($mail);
        }
        $this->assertInstanceOf(\ActualMailer\Batch::class, $batch);
        $this->assertObjectHasAttribute('batch', $batch);
        $this->assertContainsOnlyInstancesOf(\ActualMailer\Mail::class, $batch->getBatch());
        $batch->send();
    }
}